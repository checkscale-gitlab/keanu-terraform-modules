## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| alias | The display name of the alias. The name must start with the word `alias` followed by a forward slash | string | `""` | no |
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| deletion\_window\_in\_days | Duration in days after which the key is deleted after destruction of the resource | string | `"10"` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name` and `attributes` | string | `"-"` | no |
| description | The description of the key as viewed in AWS console | string | `"Parameter Store KMS master key"` | no |
| enable\_key\_rotation | Specifies whether key rotation is enabled | string | `"true"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`) | string | n/a | yes |
| name | Application or solution name (e.g. `app`) | string | n/a | yes |
| namespace | Namespace (e.g. `cp` or `cloudposse`) | string | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| alias\_arn | Alias ARN |
| alias\_name | Alias name |
| key\_arn | Key ARN |
| key\_id | Key ID |

