variable "region" {
  type        = string
  description = "The region where the bastion is to be placed"
}

variable "bastion_ami" {
  type        = string
  description = "The ami id that the basation instance will use"
}

variable "ssh_user" {
  type        = string
  description = "The ssh user used in the ami"
  default     = "admin"
}

variable "public_keys_bucket" {
  type        = string
  description = "The S3 bucket name from which ssh keys will be fetched"
}

variable "cloudflare_zone_id" {
  type        = string
  description = "The cloudflare zone id to add the dns record to"
}

variable "domain_name" {
  type        = string
  description = "The full domain name to create in the cloudflare zone (e.g, `neo.keanu.im`)"
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}
