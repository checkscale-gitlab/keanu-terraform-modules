variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = "string"
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "region" {
  description = "The AWS region to deploy to"
  default     = ""
  type        = "string"
}

variable "az" {
  description = "The AWS Availability Zone (AZ) to create the instance in"
  type        = "string"
}

variable "volume_type" {
  default     = "gp2"
  description = "Type of EBS volume to use for the EBS block device"
  type        = "string"
}

variable "size" {
  default     = "15"
  description = "Size (in GB) of EBS volume to use for the EBS block device"
  type        = "string"
}

variable "snapshot_id" {
  default     = ""
  description = "The ID of the snapshot to base the EBS block device on"
  type        = "string"
}

variable "encrypted" {
  default     = "true"
  description = "Boolean, whether or not to encrypt the EBS block device"
  type        = "string"
}

variable "kms_key_id" {
  default     = ""
  description = "ID of the KMS key to use when encyprting the EBS block device"
  type        = "string"
}

variable "iops" {
  default     = ""
  description = "The amount of IOPS to provision for the EBS block device"
  type        = "string"
}
